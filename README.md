# Tugas Kecil IF 3130 Jaringan Komputer

## CIDR (Classless Inter-Domain Routing) Caluclator

<br>
#### 13515069 - Hisham Lazuardi Yusuf - K03
<br>

### Petunjuk Penggunaan Program
1. Untuk menjalankan program, cukup dengan mengetikkan *command* `make run` ke dalam *terminal*
```
make run
```
`NB : Python tidak memerlukan kompilasi sebelum program akan dieksekusi, program hanya akan di-interpretasikan dan kemudian program akan dapat secara langsung dieksekusi. Sehinnga untuk program dalam bahasa Python hanya dibutuhkan command make run`

2. Program akan langsung dieksekusi dan akan terhubung dengan **hmif.cf** *port* **9999** dan diharuskan untuk memasukkan identitas **NIM** untuk menjawab soal

3. Program akan menjawab soal secara otomatis untuk setiap **Phase**

### Proses Pengerjaan
#### Phase 1
- Setiap input host yang didapat dari soal maka, maka **24 digit** dari *host* yang berada di depan tetap seperti input yang diterima dan merubah **8 digit** di belakang menjadi 0 dan menambah nilai **CIDR** = 24. Maka subnet yang diberikan pasti dapat menampung **256 host**.

- Misalkan input yang diterima **192.168.123.24** maka dengan subnet **192.168.123.0/24**, address(host) tersebut masuk di dalam subnet yang diberikan karena subnet tersebut dapat mengakomodir **256 host** yaitu dari range *IP* **192.168.123.0** sampai **192.168.123.255**.

#### Phase 2
- Pada soal ini, input yang diberikan berupa **subnet** yang mengandung **network ip** dan nilai **CIDR**. Kita hanya perlu mengambil nilai **CIDR** saja. Dan untuk menghitung banyaknya *host* yang dapat ditampung oleh subnet tersebut yaitu ***Number of Host*** = (1 << (32 - **CIDR**)).

#### Phase 3
- Pada soal ini, program akan mengecek apakah **host** yang diberikan berada di dalam suatu **subnet** tertentu. Fungsi ***isSubnetValid*** menerima 2 parameter yaitu **subnet** dan **host**. **Subnet** yang telah didapat akan dikonversi menjadi *ip_lower* yang merupakan batas bawah *IP address* yang dapat ditampung di dalam **subnet** dan *ip_upper* yang merupakan batas atas *IP_address* yang dikonversi ke dalam tipe ***Long int***.

- Selanjutnya fungsi akan membandingkan *IP Host* yang telah dikonversi menjadi ***Long int*** apakah berada diantara nilai dari *ip_lower* dan *ip_upper*. Jika berada diantara batas tersebut maka fungsi akan mereturn **T**, selain itu fungsi akan mereturn **F**
