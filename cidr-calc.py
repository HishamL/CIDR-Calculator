# Nama/NIM : Hisham Lazuardi Yusuf / 13515069
# Nama File : cidr-calc.py
# IF 3130 Jaringan Komputer
# Tugas Kecil I
# CIDR (Classless Inter-Domain Routing) Calculato
#!/usr/bin/env python

import socket, binascii

# Fungsi untuk melakukan pembacaan
def recv_until(conn, str):
  buf = ''
  while not str in buf:
    buf += conn.recv(1)
  return buf

# Fungsi untuk melakukan perhitungan Valid subnet
def getValidSubnet(host):
  address = host.split('.')
  subnet = ''
  i = 0
  for net in address:
    if (i < 3):
      subnet += net + '.'
    i += 1
  subnet += '0/24'
  return subnet

# Fungsi untuk melakukan perhitungan banyaknya host
def countHosts(subnet):
  addr = subnet.split('/')
  cidr = 32 - int(addr[1])
  numofhost = str(1 << cidr)
  return numofhost

# Fungsi untuk mengecek apakah subnet yang diberikan valid
def isSubnetValid(subnet, host):
  net = subnet.split('/')
  network_prefix = net[0]
  netmask_len = int(net[1])
  ip_len = 32

  # Mencari batas bawah dan batas atas IP dalam long int
  mask = (1 << (ip_len - netmask_len)) - 1
  netmask = ((1 << ip_len) - 1) - mask
  ip_hex = socket.inet_pton(socket.AF_INET, network_prefix)
  ip_lower = int(binascii.hexlify(ip_hex), 16) & netmask
  ip_upper = ip_lower + mask

  # Konversi IP host menjadi long int
  host_ip_hex = socket.inet_pton(socket.AF_INET, host)
  host_ip = int(binascii.hexlify(host_ip_hex), 16)

  if (ip_lower <= host_ip <= ip_upper):
    return 'T'
  else:
    return 'F'

# Initialization
TCP_IP = 'hmif.cf'
TCP_PORT = 9999

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

data = recv_until(s, 'NIM: ')
nim = raw_input(data)
s.send(nim + '\n')

data = recv_until(s, 'Verify NIM: ')
nim = raw_input(data)
s.send(nim + '\n')

print recv_until(s, '\n')[:-1]

# Solution
# Phase 1
for i in range(100):
	recv_until(s, 'Host: ')
	host = recv_until(s, '\n')[:-1]
  print "Phase 1 = " + i
	recv_until(s, 'Subnet: ')
	s.send(getValidSubnet(host) + '\n')
print recv_until(s, '\n')[:-1]

# Phase 2
for i in range(100):
	recv_until(s, 'Subnet: ')
	subnet = recv_until(s, '\n')[:-1]
  print "Phase 2 = " + i
	recv_until(s, 'Number of Hosts: ')
	s.send(countHosts(subnet) + '\n')
print recv_until(s, '\n')[:-1]

# Phase 3
for i in range(100):
	recv_until(s, 'Subnet: ')
	subnet = recv_until(s, '\n')[:-1]
  print "Phase 3 = " + i
	recv_until(s, 'Host: ')
	host = recv_until(s, '\n')[:-1]
	s.send(isSubnetValid(subnet, host) + '\n')
print recv_until(s, '\n')[:-1]

s.close()
